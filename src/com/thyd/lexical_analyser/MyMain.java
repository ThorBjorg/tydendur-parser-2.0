package com.thyd.lexical_analyser;

import com.thyd.Parser;

import java.io.*;
import java.util.*;

public class MyMain {
    public static void main(String [] args) throws IOException {
        LinkedList<Token> tokenStream = new LinkedList<Token>();
        Parser parser = new Parser();
        Lexer lexer = new Lexer(new FileReader(args[0]));

        boolean first = true;
        while(true) {
            if (first)
                first = false;
                Token t = lexer.yylex();
            tokenStream.add(t);
            if (t != null && t.getTokenCode() == TokenCode.EOF)
                break;
        }
        parser.start(tokenStream);
        List<ErrorHandler> errors = parser.getErrorList();
        List<String> DocumentLines = new ArrayList<String>();
        BufferedReader br = new BufferedReader(new FileReader(args[0]));
        String line;
        while ((line = br.readLine()) != null) {
            DocumentLines.add(line);
        }
        br.close();

        boolean isEnd = false;
        for (ErrorHandler error : errors) {
            String ErrorReport;
            if(error.getGot() == TokenCode.ERR_ILL_CHAR) {
                ErrorReport = " Illegal character.";
            }
            else if (error.getExpected() == TokenCode.NOMATCH) {
                ErrorReport = " Illegal code in program for token: " + TokenCodeToString(error.getGot());
            }
            else if(error.getGot() == TokenCode.EOF) {
                isEnd = true;
                ErrorReport = " Missing " + TokenCodeToString(TokenCode.RBRACE);              //Spurning um ad baeta vid herna rettar linu, eof kemur alltaf sem line 0 column 1
            }
            else {
                ErrorReport = " Expected: " + TokenCodeToString(error.getExpected()) + ", Got: " + TokenCodeToString(error.getGot());
            }

            if (error.getLine() < 10) {
                System.out.println(" " + (error.getLine() + 1) + " : " + DocumentLines.get(error.getLine()));
            } else {
                if (!isEnd) {
                    System.out.println((error.getLine() + 1) + " : " + DocumentLines.get(error.getLine()));
                }
                else {
                    System.out.println((error.getLine() + 1) + " : ");
                }
            }
            String hat = "";
            for (int i = 0; i < error.getColumn() + 5; i++) {
                hat = hat + " ";
            }
            hat = hat + "^";
            System.out.println(hat + ErrorReport);

        }
        System.out.println("Number of Errors " + errors.size());
    }

    public static String TokenCodeToString(TokenCode token){
        switch (token){
            case IDENTIFIER:
                return "Identifier";

            case NUMBER:
                return "Number";

            case INCDECOP:
                return "Inc/Dec operator";

            case RELOP:
                return "Relational operator";

            case MULOP:
                return "Multiplication operator";

            case ADDOP:
                return "Addition operator";

            case ASSIGNOP:
                return "Assign operator";

            case CLASS:
                return "Class";

            case VOID:
                return "Void";

            case IF:
                return "If";

            case ELSE:
                return "Else";

            case FOR:
                return "For";

            case RETURN:
                return "Return";

            case BREAK:
                return "Break";

            case CONTINUE:
                return "Continue";

            case LBRACE:
                return "Left Brace {";                      //hvort vilji tid hafa? symbolid eda textann

            case RBRACE:
                return "Right Brace }";

            case LBRACKET:
                return "Left Bracket [";                 //  |
            //   v
            case RBRACKET:
                return "Right Bracket ]";

            case LPAREN:
                return "Left Parenthesis (";

            case RPAREN:                              //    |
                return "Right Parenthesis )";         //    v ???

            case SEMICOLON:
                return ";";

            case COMMA:                               //    |
                return ",";                           //    v

            case NOT:
                return "!";                              //???????????????????????????????????

            case INT:
                return "Type";

            case REAL:
                return "Type";

            case STATIC:
                return "Static";

            case EOF:
                return "End of File";

            case ERR_ILL_CHAR:
                return "Illegal Character";

            case ERR_LONG_ID:
                return "Identifier to Long";

            case EPSILON:
                return "?  ?";                      //kemur tetta einhverntiman fyrir?

            case NOMATCH:
                return "Parsing Error";             //thid radid hvad ti viljid hafa

        }
        return "";
    }
}