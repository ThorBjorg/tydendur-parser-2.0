package com.thyd.lexical_analyser;

public enum DataType {
  NONE, INT, REAL, ID, KEYWORD, OP
}