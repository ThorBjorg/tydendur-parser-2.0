package com.thyd.lexical_analyser;

/**
 * Created by Sisso on 13-Oct-14.
 */
public class ErrorHandler {
    private TokenCode expected;
    private TokenCode got;
    private int line;
    private int column;

    public ErrorHandler(TokenCode _expected, TokenCode _got, int _line, int _column){
        expected = _expected;
        got = _got;
        line = _line;
        column = _column;
    }

    public TokenCode getExpected() {
        return expected;
    }

    public TokenCode getGot() {
        return got;
    }

    public int getLine() {
        return line;
    }

    public int getColumn() {
        return column;
    }


}
