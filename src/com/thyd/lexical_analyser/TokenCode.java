package com.thyd.lexical_analyser;

public enum TokenCode {
  IDENTIFIER, NUMBER, INCDECOP, RELOP, MULOP, ADDOP,
  ASSIGNOP,
  CLASS, VOID, IF, ELSE, FOR, RETURN, BREAK, CONTINUE, 
  LBRACE, RBRACE, LBRACKET, RBRACKET, LPAREN, RPAREN,
  SEMICOLON, COMMA, NOT, INT, REAL,STATIC,
  EOF, ERR_ILL_CHAR, ERR_LONG_ID,EPSILON, NOMATCH

}