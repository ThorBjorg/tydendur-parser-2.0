package com.thyd.lexical_analyser.sets;

import com.thyd.lexical_analyser.NonTerminals;
import com.thyd.lexical_analyser.TokenCode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Sisso on 13-Oct-14.
 */
public class Follow {
    protected HashMap<NonTerminals,List<TokenCode>> FOLLOW;

    public Follow(){
        FOLLOW = new HashMap<NonTerminals, List<TokenCode>>();
        setFollow();
    }

    private void setFollow(){

        FOLLOW.put(NonTerminals.PROGRAM, new ArrayList<TokenCode>(){{
            add(TokenCode.EOF);
        }});

        FOLLOW.put(NonTerminals.VARIABLE_DECLARATIONS, new ArrayList<TokenCode>(){{
            add(TokenCode.STATIC);
        }});

        FOLLOW.put(NonTerminals.TYPE, new ArrayList<TokenCode>(){{
            add(TokenCode.IDENTIFIER);
        }});

        FOLLOW.put(NonTerminals.VARIABLE_LIST, new ArrayList<TokenCode>(){{
            add(TokenCode.SEMICOLON);
        }});

        FOLLOW.put(NonTerminals.VARIABLE_LIST_2, new ArrayList<TokenCode>(){{
            add(TokenCode.SEMICOLON);
        }});

        FOLLOW.put(NonTerminals.VARIABLE, new ArrayList<TokenCode>(){{
            add(TokenCode.SEMICOLON);
            add(TokenCode.COMMA);
        }});

        FOLLOW.put(NonTerminals.VARIABLE_2, new ArrayList<TokenCode>(){{
            add(TokenCode.SEMICOLON);
            add(TokenCode.COMMA);
        }});

        FOLLOW.put(NonTerminals.METHOD_DECLARATIONS, new ArrayList<TokenCode>(){{
            add(TokenCode.RBRACE);
        }});

        FOLLOW.put(NonTerminals.MORE_METHOD_DECLARATIONS, new ArrayList<TokenCode>(){{
            add(TokenCode.RBRACE);
        }});

        FOLLOW.put(NonTerminals.METHOD_DECLARATION, new ArrayList<TokenCode>(){{
            add(TokenCode.RBRACE);
            add(TokenCode.STATIC);

        }});

        FOLLOW.put(NonTerminals.METHOD_RETURN_TYPE, new ArrayList<TokenCode>(){{
            add(TokenCode.IDENTIFIER);
        }});

        FOLLOW.put(NonTerminals.PARAMETERS, new ArrayList<TokenCode>(){{
            add(TokenCode.RPAREN);
        }});

        FOLLOW.put(NonTerminals.PARAMETER_LIST, new ArrayList<TokenCode>(){{
            add(TokenCode.RPAREN);
        }});

        FOLLOW.put(NonTerminals.PARAMETER_LIST_2, new ArrayList<TokenCode>(){{
            add(TokenCode.RPAREN);
        }});

        FOLLOW.put(NonTerminals.STATEMENT_LIST, new ArrayList<TokenCode>(){{
            add(TokenCode.RBRACE);
        }});

        FOLLOW.put(NonTerminals.STATEMENT, new ArrayList<TokenCode>(){{
            add(TokenCode.IDENTIFIER);
            add(TokenCode.IF);
            add(TokenCode.FOR);
            add(TokenCode.RETURN);
            add(TokenCode.BREAK);
            add(TokenCode.CONTINUE);
            add(TokenCode.RBRACE);
            add(TokenCode.LBRACE);
        }});

        FOLLOW.put(NonTerminals.SUPER_AWESOME, new ArrayList<TokenCode>(){{
            add(TokenCode.IDENTIFIER);
            add(TokenCode.IF);
            add(TokenCode.FOR);
            add(TokenCode.RETURN);
            add(TokenCode.BREAK);
            add(TokenCode.CONTINUE);
            add(TokenCode.RBRACE);
            add(TokenCode.LBRACE);
        }});

        FOLLOW.put(NonTerminals.SUPER_AWESOME_2, new ArrayList<TokenCode>(){{
            add(TokenCode.MULOP);
            add(TokenCode.ADDOP);
            add(TokenCode.RELOP);
            add(TokenCode.RBRACKET);
            add(TokenCode.RPAREN);
            add(TokenCode.COMMA);
            add(TokenCode.SEMICOLON);
        }});

        FOLLOW.put(NonTerminals.SUPER_AWESOME_3, new ArrayList<TokenCode>(){{
            add(TokenCode.IDENTIFIER);
            add(TokenCode.IF);
            add(TokenCode.FOR);
            add(TokenCode.RETURN);
            add(TokenCode.BREAK);
            add(TokenCode.CONTINUE);
            add(TokenCode.RBRACE);
            add(TokenCode.LBRACE);
        }});

        FOLLOW.put(NonTerminals.OPTIONAL_EXPRESSION, new ArrayList<TokenCode>(){{
            add(TokenCode.SEMICOLON);
         }});

        FOLLOW.put(NonTerminals.STATEMENT_BLOCK, new ArrayList<TokenCode>(){{
            add(TokenCode.IDENTIFIER);
            add(TokenCode.IF);
            add(TokenCode.FOR);
            add(TokenCode.RETURN);
            add(TokenCode.BREAK);
            add(TokenCode.CONTINUE);
            add(TokenCode.RBRACE);
            add(TokenCode.LBRACE);
            add(TokenCode.ELSE);

        }});

        FOLLOW.put(NonTerminals.INCR_DECR_VAR_ORIGINAL, new ArrayList<TokenCode>(){{
            add(TokenCode.RPAREN);
        }});

        FOLLOW.put(NonTerminals.OPTIONAL_ELSE, new ArrayList<TokenCode>(){{
            add(TokenCode.IDENTIFIER);
            add(TokenCode.IF);
            add(TokenCode.FOR);
            add(TokenCode.RETURN);
            add(TokenCode.BREAK);
            add(TokenCode.CONTINUE);
            add(TokenCode.RBRACE);
            add(TokenCode.LBRACE);
        }});

        FOLLOW.put(NonTerminals.EXPRESSION_LIST, new ArrayList<TokenCode>(){{
            add(TokenCode.RPAREN);
        }});

        FOLLOW.put(NonTerminals.MORE_EXPRESSIONS, new ArrayList<TokenCode>(){{
            add(TokenCode.RPAREN);
        }});

        FOLLOW.put(NonTerminals.EXPRESSION, new ArrayList<TokenCode>(){{
            add(TokenCode.RBRACKET);
            add(TokenCode.RPAREN);
            add(TokenCode.COMMA);
            add(TokenCode.SEMICOLON);
        }});

        FOLLOW.put(NonTerminals.EXPRESSION_2, new ArrayList<TokenCode>(){{
            add(TokenCode.RBRACKET);
            add(TokenCode.RPAREN);
            add(TokenCode.COMMA);
            add(TokenCode.SEMICOLON);
        }});

        FOLLOW.put(NonTerminals.SIMPLE_EXPRESSION, new ArrayList<TokenCode>(){{
            add(TokenCode.RELOP);
            add(TokenCode.RBRACKET);
            add(TokenCode.RPAREN);
            add(TokenCode.COMMA);
            add(TokenCode.SEMICOLON);
        }});

        FOLLOW.put(NonTerminals.SIMPLE_EXPRESSION_2, new ArrayList<TokenCode>(){{
            add(TokenCode.RELOP);
            add(TokenCode.RBRACKET);
            add(TokenCode.RPAREN);
            add(TokenCode.COMMA);
            add(TokenCode.SEMICOLON);
        }});

        FOLLOW.put(NonTerminals.TERM, new ArrayList<TokenCode>(){{
            add(TokenCode.ADDOP);
            add(TokenCode.RELOP);
            add(TokenCode.RBRACKET);
            add(TokenCode.RPAREN);
            add(TokenCode.COMMA);
            add(TokenCode.SEMICOLON);
        }});

        FOLLOW.put(NonTerminals.TERM_2, new ArrayList<TokenCode>(){{
            add(TokenCode.ADDOP);
            add(TokenCode.RELOP);
            add(TokenCode.RBRACKET);
            add(TokenCode.RPAREN);
            add(TokenCode.COMMA);
            add(TokenCode.SEMICOLON);
        }});

        FOLLOW.put(NonTerminals.FACTOR, new ArrayList<TokenCode>(){{
            add(TokenCode.MULOP);
            add(TokenCode.ADDOP);
            add(TokenCode.RELOP);
            add(TokenCode.RBRACKET);
            add(TokenCode.RPAREN);
            add(TokenCode.COMMA);
            add(TokenCode.SEMICOLON);
        }});

        FOLLOW.put(NonTerminals.VARIABLE_LOC, new ArrayList<TokenCode>(){{
            add(TokenCode.ASSIGNOP);
            add(TokenCode.INCDECOP);
            add(TokenCode.MULOP);
            add(TokenCode.ADDOP);
            add(TokenCode.RELOP);
            add(TokenCode.RBRACKET);
            add(TokenCode.RPAREN);
            add(TokenCode.COMMA);
            add(TokenCode.SEMICOLON);
        }});

        FOLLOW.put(NonTerminals.VARIABLE_LOC_ORIGINAL, new ArrayList<TokenCode>(){{
            add(TokenCode.INCDECOP);
            add(TokenCode.ASSIGNOP);
        }});

        FOLLOW.put(NonTerminals.VARIABLE_LOC_ORIGINAL_2, new ArrayList<TokenCode>(){{
            add(TokenCode.INCDECOP);
            add(TokenCode.ASSIGNOP);
        }});

        FOLLOW.put(NonTerminals.SIGN, new ArrayList<TokenCode>(){{
            add(TokenCode.IDENTIFIER);
            add(TokenCode.NUMBER);
            add(TokenCode.LPAREN);
            add(TokenCode.NOT);
        }});

    }

    public List<TokenCode> getFOLLOW(NonTerminals key){
        List<TokenCode> value;

        value = FOLLOW.get(key);

        return value;
    }
}
