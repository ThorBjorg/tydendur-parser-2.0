package com.thyd.lexical_analyser.sets;

import com.thyd.lexical_analyser.NonTerminals;
import com.thyd.lexical_analyser.TokenCode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Sisso on 13-Oct-14.
 */
public class Synchronized {
    protected Follow follow;
    protected First first;
    protected HashMap<NonTerminals,List<TokenCode>> sync;

    public Synchronized(){
        first = new First();
        follow = new Follow();
    }

    public List<TokenCode> getSynchronized(NonTerminals key){
        List<TokenCode> value = new ArrayList<TokenCode>();
        value.addAll(follow.getFOLLOW(key));
        switch (key) {
            case PROGRAM:
                value.addAll(program());
                break;
            case VARIABLE_DECLARATIONS:
                value.addAll(variable_declarations());
                break;
            case TYPE:
                value.addAll(type());
                break;
            case VARIABLE_LIST_2:
                value.addAll(variable_list_2());
                break;
            case VARIABLE:
                value.addAll(variable());
                break;
            case VARIABLE_2:
                value.addAll(variable_2());
                break;
            case METHOD_DECLARATIONS:
                value.addAll(method_declarations());
                break;
            case MORE_METHOD_DECLARATIONS:
                value.addAll(more_method_declarations());
                break;
            case METHOD_DECLARATION:
                value.addAll(method_declarations());
                break;
            case METHOD_RETURN_TYPE:
                value.addAll(method_return_type());
                break;
            case PARAMETERS:
                value.addAll(parameters());
                break;
            case PARAMETER_LIST_2:
                value.addAll(parameter_list_2());
                break;
            case STATEMENT_LIST:
                value.addAll(statement_list());
                break;
            case STATEMENT:
                value.addAll(statement());
                break;
            case SUPER_AWESOME:
                value.addAll(super_awesome());
                break;
            case OPTIONAL_EXPRESSION:
                value.addAll(optional_expression());
                break;
            case STATEMENT_BLOCK:
                value.addAll(statement_block());
                break;
            case OPTIONAL_ELSE:
                value.addAll(optional_else());
                break;
            case EXPRESSION_LIST:
                value.addAll(expression_list());
                break;
            case MORE_EXPRESSIONS:
                value.addAll(more_expressions());
                break;
            case EXPRESSION_2:
                value.addAll(expression_2());
                break;
            case SIMPLE_EXPRESSION_2:
                value.addAll(simple_expression_2());
                break;
            case TERM_2:
                value.addAll(term_2());
                break;
            case FACTOR:
                value.addAll(factor());
                break;
            case VARIABLE_LOC:
                value.addAll(variable_loc());
                break;
            case SIGN:
                value.addAll(sign());
                break;
            case VARIABLE_LIST:
                value.addAll(variable_list());
                break;
            case PARAMETER_LIST:
                value.addAll(parameter_list());
                break;
            case TERM:
                value.addAll(term());
                break;
            case SIMPLE_EXPRESSION:
                value.addAll(simple_expression());
                break;
            case EXPRESSION:
                value.addAll(expression());
                break;
            case VARIABLE_LOC_ORIGINAL_2:
                value.addAll(variable_loc_original_2());
                break;
            case VARIABLE_LOC_ORIGINAL:
                value.addAll(variable_loc_original());
                break;
            case SUPER_AWESOME_3:
                value.addAll(super_awesome_3());
                break;
            case INCR_DECR_VAR_ORIGINAL:
                value.addAll(incr_decr_var_original());
                break;
            case SUPER_AWESOME_2:
                value.addAll(super_awesome_2());
                break;
        }
        return value;
    }

    private List<TokenCode> variable_list_2() {
        return null;
    }

    private List<TokenCode> variable() {
        return null;
    }

    private List<TokenCode> variable_2() {
        return null;
    }

    private List<TokenCode> method_declarations() {
        return null;
    }

    private List<TokenCode> more_method_declarations() {
        return null;
    }

    private List<TokenCode> method_return_type() {
        return null;
    }

    private List<TokenCode> parameters() {
        return null;
    }

    private List<TokenCode> parameter_list_2() {
        return null;
    }

    private List<TokenCode> statement_list() {
        return null;
    }

    private List<TokenCode> statement() {
        return null;
    }

    private List<TokenCode> super_awesome() {
        return null;
    }

    private List<TokenCode> optional_expression() {
        return null;
    }

    private List<TokenCode> statement_block() {
        return null;
    }

    private List<TokenCode> optional_else() {
        return null;
    }

    private List<TokenCode> expression_list() {
        return null;
    }

    private List<TokenCode> more_expressions() {
        return null;
    }

    private List<TokenCode> expression_2() {
        return null;
    }

    private List<TokenCode> simple_expression_2() {
        return null;
    }

    private List<TokenCode> term_2() {
        return null;
    }

    private List<TokenCode> factor() {
        return null;
    }

    private List<TokenCode> variable_loc() {
        return null;
    }

    private List<TokenCode> sign() {
        return null;
    }

    private List<TokenCode> variable_list() {
        return null;
    }

    private List<TokenCode> parameter_list() {
        return null;
    }

    private List<TokenCode> term() {
        return null;
    }

    private List<TokenCode> simple_expression() {
        return null;
    }

    private List<TokenCode> expression() {
        return null;
    }

    private List<TokenCode> variable_loc_original_2() {
        return null;
    }

    private List<TokenCode> variable_loc_original() {
        return null;
    }

    private List<TokenCode> super_awesome_3() {
        return null;
    }

    private List<TokenCode> incr_decr_var_original() {
        return null;
    }

    private List<TokenCode> super_awesome_2() {
        return null;
    }

    private List<TokenCode> type() {
        return null;
    }

    private List<TokenCode> variable_declarations() {
        return null;
    }

    private List<TokenCode> program() {
        return null;
    }
}
