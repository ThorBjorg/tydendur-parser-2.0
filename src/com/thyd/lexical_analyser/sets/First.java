package com.thyd.lexical_analyser.sets;

import com.thyd.lexical_analyser.NonTerminals;
import com.thyd.lexical_analyser.TokenCode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Sisso on 13-Oct-14.
 */
public class First {

    protected HashMap<NonTerminals,List<TokenCode>> FIRST;

    public First(){
        FIRST = new HashMap<NonTerminals, List<TokenCode>>();
        setFirst();
    }


    private void setFirst(){

        FIRST.put(NonTerminals.PROGRAM, new ArrayList<TokenCode>(){{
            add(TokenCode.CLASS);
        }});

        FIRST.put(NonTerminals.VARIABLE_DECLARATIONS, new ArrayList<TokenCode>(){{
            add(TokenCode.INT);
            add(TokenCode.REAL);
            add(TokenCode.EPSILON);
        }});

        FIRST.put(NonTerminals.TYPE, new ArrayList<TokenCode>(){{
            add(TokenCode.INT);
            add(TokenCode.REAL);
        }});

        FIRST.put(NonTerminals.VARIABLE_LIST_2, new ArrayList<TokenCode>(){{
            add(TokenCode.COMMA);
            add(TokenCode.EPSILON);
        }});

        FIRST.put(NonTerminals.VARIABLE, new ArrayList<TokenCode>(){{
            add(TokenCode.IDENTIFIER);
        }});

        FIRST.put(NonTerminals.VARIABLE_2, new ArrayList<TokenCode>(){{
            add(TokenCode.LBRACKET);
            add(TokenCode.EPSILON);
        }});

        FIRST.put(NonTerminals.METHOD_DECLARATIONS, new ArrayList<TokenCode>(){{
            add(TokenCode.STATIC);
        }});

        FIRST.put(NonTerminals.MORE_METHOD_DECLARATIONS, new ArrayList<TokenCode>(){{
            add(TokenCode.STATIC);
            add(TokenCode.EPSILON);
        }});

        FIRST.put(NonTerminals.METHOD_DECLARATION, new ArrayList<TokenCode>(){{
            add(TokenCode.STATIC);
        }});

        FIRST.put(NonTerminals.METHOD_RETURN_TYPE, new ArrayList<TokenCode>(){{
            add(TokenCode.VOID);
            add(TokenCode.INT);
            add(TokenCode.REAL);
        }});

        FIRST.put(NonTerminals.PARAMETERS, new ArrayList<TokenCode>(){{
            add(TokenCode.INT);
            add(TokenCode.REAL);
            add(TokenCode.EPSILON);

        }});

        FIRST.put(NonTerminals.PARAMETER_LIST_2, new ArrayList<TokenCode>(){{
            add(TokenCode.COMMA);
            add(TokenCode.EPSILON);
        }});

        FIRST.put(NonTerminals.STATEMENT_LIST, new ArrayList<TokenCode>(){{
            add(TokenCode.IDENTIFIER);
            add(TokenCode.IF);
            add(TokenCode.FOR);
            add(TokenCode.RETURN);
            add(TokenCode.BREAK);
            add(TokenCode.CONTINUE);
            add(TokenCode.LBRACE);
            add(TokenCode.EPSILON);
        }});

        FIRST.put(NonTerminals.STATEMENT, new ArrayList<TokenCode>(){{
            add(TokenCode.IDENTIFIER);
            add(TokenCode.IF);
            add(TokenCode.FOR);
            add(TokenCode.RETURN);
            add(TokenCode.BREAK);
            add(TokenCode.CONTINUE);
            add(TokenCode.LBRACE);
        }});

        FIRST.put(NonTerminals.SUPER_AWESOME, new ArrayList<TokenCode>(){{
            add(TokenCode.LPAREN);
            add(TokenCode.ASSIGNOP);
            add(TokenCode.LBRACKET);
            add(TokenCode.INCDECOP);
            add(TokenCode.EPSILON);
        }});

        FIRST.put(NonTerminals.SUPER_AWESOME_2, new ArrayList<TokenCode>(){{
            add(TokenCode.LPAREN);
            add(TokenCode.LBRACKET);
            add(TokenCode.EPSILON);

        }});

        FIRST.put(NonTerminals.SUPER_AWESOME_3, new ArrayList<TokenCode>(){{
            add(TokenCode.ASSIGNOP);
            add(TokenCode.INCDECOP);
        }});

        FIRST.put(NonTerminals.OPTIONAL_EXPRESSION, new ArrayList<TokenCode>(){{
            add(TokenCode.IDENTIFIER);
            add(TokenCode.NUMBER);
            add(TokenCode.LPAREN);
            add(TokenCode.NOT);
            add(TokenCode.ADDOP);
            add(TokenCode.EPSILON);

        }});

        FIRST.put(NonTerminals.STATEMENT_BLOCK, new ArrayList<TokenCode>(){{
            add(TokenCode.LBRACE);
        }});

        FIRST.put(NonTerminals.OPTIONAL_ELSE, new ArrayList<TokenCode>(){{
            add(TokenCode.ELSE);
            add(TokenCode.EPSILON);
        }});

        FIRST.put(NonTerminals.EXPRESSION_LIST, new ArrayList<TokenCode>(){{
            add(TokenCode.IDENTIFIER);
            add(TokenCode.NUMBER);
            add(TokenCode.LPAREN);
            add(TokenCode.NOT);
            add(TokenCode.ADDOP);
            add(TokenCode.EPSILON);
        }});

        FIRST.put(NonTerminals.MORE_EXPRESSIONS, new ArrayList<TokenCode>(){{
            add(TokenCode.COMMA);
            add(TokenCode.EPSILON);
        }});

        FIRST.put(NonTerminals.EXPRESSION_2, new ArrayList<TokenCode>(){{
            add(TokenCode.RELOP);
            add(TokenCode.EPSILON);
        }});

        FIRST.put(NonTerminals.SIMPLE_EXPRESSION_2, new ArrayList<TokenCode>(){{
            add(TokenCode.ADDOP);
            add(TokenCode.EPSILON);
        }});

        FIRST.put(NonTerminals.TERM_2, new ArrayList<TokenCode>(){{
            add(TokenCode.MULOP);
            add(TokenCode.EPSILON);
        }});

        FIRST.put(NonTerminals.FACTOR, new ArrayList<TokenCode>(){{
            add(TokenCode.IDENTIFIER);
            add(TokenCode.NUMBER);
            add(TokenCode.LPAREN);
            add(TokenCode.NOT);
        }});

        FIRST.put(NonTerminals.VARIABLE_LOC, new ArrayList<TokenCode>(){{
            add(TokenCode.LBRACKET);
            add(TokenCode.EPSILON);
        }});

        FIRST.put(NonTerminals.VARIABLE_LOC_ORIGINAL, new ArrayList<TokenCode>(){{
            add(TokenCode.IDENTIFIER);
        }});

        FIRST.put(NonTerminals.VARIABLE_LOC_ORIGINAL_2, new ArrayList<TokenCode>(){{
            add(TokenCode.LBRACKET);
            add(TokenCode.EPSILON);
        }});

        FIRST.put(NonTerminals.SIGN, new ArrayList<TokenCode>(){{
            add(TokenCode.ADDOP);                                        //gaeti turft ad breyta, ihuga
        }});

        FIRST.put(NonTerminals.VARIABLE_LIST, new ArrayList<TokenCode>(){{
            add(TokenCode.IDENTIFIER);
        }});

        FIRST.put(NonTerminals.PARAMETER_LIST, new ArrayList<TokenCode>(){{
            add(TokenCode.INT);
            add(TokenCode.REAL);
        }});

        FIRST.put(NonTerminals.INCR_DECR_VAR_ORIGINAL, new ArrayList<TokenCode>(){{
            add(TokenCode.IDENTIFIER);
        }});

        FIRST.put(NonTerminals.TERM, new ArrayList<TokenCode>(){{
            add(TokenCode.IDENTIFIER);
            add(TokenCode.NUMBER);
            add(TokenCode.LPAREN);
            add(TokenCode.NOT);
        }});

        FIRST.put(NonTerminals.SIMPLE_EXPRESSION, new ArrayList<TokenCode>(){{
            add(TokenCode.IDENTIFIER);
            add(TokenCode.NUMBER);
            add(TokenCode.LPAREN);
            add(TokenCode.NOT);
            add(TokenCode.ADDOP);
        }});

        FIRST.put(NonTerminals.EXPRESSION, new ArrayList<TokenCode>(){{
            add(TokenCode.IDENTIFIER);
            add(TokenCode.NUMBER);
            add(TokenCode.LPAREN);
            add(TokenCode.NOT);
            add(TokenCode.ADDOP);
        }});

    }

    public List<TokenCode> getFIRST(NonTerminals key){
        List<TokenCode> value;

        value = FIRST.get(key);

        return value;

    }
}
