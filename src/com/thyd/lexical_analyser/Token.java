package com.thyd.lexical_analyser;

public class Token {
  private TokenCode m_tc;
  private DataType m_dt;
  private OpType m_ot;
  private SymbolTableEntry m_ste;
  private int m_line;
  private int m_column;

  public Token(TokenCode tc) {
    this(tc, DataType.NONE, OpType.NONE);
  }

  public Token(TokenCode tc, DataType dt, OpType ot) {
    this(tc, dt, ot, null);
  }

  public Token(TokenCode tc, DataType dt, OpType ot, SymbolTableEntry ste) {
    m_tc = tc;
    m_dt = dt;
    m_ot = ot;
    m_ste = ste;
  }
    public Token(TokenCode tc,int line,int column) {
        this(tc, DataType.NONE, OpType.NONE);
        this.m_line = line;
        this.m_column = column;
    }

    public Token(TokenCode tc, DataType dt, OpType ot,int line,int column) {
        this(tc, dt, ot, null);
        this.m_line = line;
        this.m_column = column;
    }

    public Token(TokenCode tc, DataType dt, OpType ot, SymbolTableEntry ste,int line,int column) {
        m_tc = tc;
        m_dt = dt;
        m_ot = ot;
        m_ste = ste;
        this.m_line = line;
        this.m_column = column;
    }


    public static Token createRaw(TokenCode keywordTokenCode) {
        return new Token(keywordTokenCode, DataType.NONE, OpType.NONE);
    }
    public static Token createRaw(TokenCode keywordTokenCode,int line,int column) {
    return new Token(keywordTokenCode, DataType.NONE, OpType.NONE,line,column);
  }

  public static Token createOp(TokenCode opTokenCode, OpType opType,int line,int column) {
    return new Token(opTokenCode, DataType.OP, opType,line,column);
  }

  public static Token createId(SymbolTableEntry symTabEntry,int line,int column) {
    return new Token(TokenCode.IDENTIFIER, DataType.ID, OpType.NONE, symTabEntry,line,column);
  }

  public static Token createInt(SymbolTableEntry symTabEntry,int line,int column) {
    return new Token(TokenCode.NUMBER, DataType.INT, OpType.NONE, symTabEntry,line,column);
  }

  public static Token createReal(SymbolTableEntry symTabEntry,int line,int column) {
    return new Token(TokenCode.NUMBER, DataType.REAL, OpType.NONE, symTabEntry,line,column);
  }

  public static Token createRelOp(String lexeme,int line,int column) {
    OpType opType = OpType.NONE;
    if (lexeme.equals("=="))
      opType = OpType.EQUAL;
    else if (lexeme.equals("!="))
      opType = OpType.NOT_EQUAL;
    else if (lexeme.equals("<"))
      opType = OpType.LT;
    else if (lexeme.equals(">"))
      opType = OpType.GT;
    else if (lexeme.equals("<="))
      opType = OpType.LTE;
    else if (lexeme.equals("=="))
      opType = OpType.GTE;
    return Token.createOp(TokenCode.RELOP, opType,line,column);
  }

  public static Token createMulOp(String lexeme,int line,int column) {
    OpType opType = OpType.NONE;
    if (lexeme.equals("*"))
      opType = OpType.MULT;
    else if (lexeme.equals("/"))
      opType = OpType.DIV;
    else if (lexeme.equals("%"))
      opType = OpType.MOD;
    else if (lexeme.equals("&&"))
      opType = OpType.AND;
    return Token.createOp(TokenCode.MULOP, opType,line,column);
  }


  public static Token createAddOp(String lexeme,int line,int column) {
    OpType opType = OpType.NONE;
    if (lexeme.equals("+"))
      opType = OpType.PLUS;
    else if (lexeme.equals("-"))
      opType = OpType.MINUS;
    else if (lexeme.equals("||"))
      opType = OpType.OR;
    return Token.createOp(TokenCode.ADDOP, opType,line,column);
  }

  public TokenCode getTokenCode() {
    return m_tc;
  }

    public int getM_line() {
        return m_line;
    }

    public int getM_column() {
        return m_column;
    }

    public DataType getDataType() {
    return m_dt;
  }

  public OpType getOpType() {
    return m_ot;
  }

  public SymbolTableEntry getSymTabEntry() {
    return m_ste;
  }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Token token = (Token) o;

        if (m_tc != token.m_tc) return false;

        return true;
    }

    public boolean matchTokenCode(TokenCode tokenCode) {
        if (this.m_tc.equals(tokenCode)) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return m_tc.hashCode();
    }
}