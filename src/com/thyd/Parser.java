package com.thyd;

import com.thyd.lexical_analyser.ErrorHandler;
import com.thyd.lexical_analyser.NonTerminals;
import com.thyd.lexical_analyser.Token;
import com.thyd.lexical_analyser.TokenCode;
import com.thyd.lexical_analyser.sets.First;
import com.thyd.lexical_analyser.sets.Follow;
import com.thyd.lexical_analyser.sets.Synchronized;

import java.util.*;


public class Parser {

    protected LinkedList<Token> m_Token_Stack;
    protected Object[] m_DEBUG_Token_Stack;
    protected Token lookahead;
    protected TokenCode lastCheck;
    protected Stack<NonTerminals> m_CallStack = new Stack<NonTerminals>();
    protected First m_first = new First();
    protected Follow m_follow = new Follow();
    protected List<TokenCode> m_synchronizedSet = new LinkedList<TokenCode>();
    protected List<ErrorHandler> m_errors = new ArrayList<ErrorHandler>();

    protected boolean panicMode;
    protected int numErrors;

    public Parser() {
    }

    public static void main(String[] args) {
	// write your code here
    }


    public void start (LinkedList<Token> stack) {
        m_Token_Stack = stack;
        m_DEBUG_Token_Stack = stack.toArray();
        lookahead = m_Token_Stack.poll();
        if (lookahead.getTokenCode() != TokenCode.EOF) {
            program();
        }

    }
    private void program() {
        m_CallStack.push(NonTerminals.PROGRAM);
        if (checkTokenCode(TokenCode.CLASS)) {
            matchTokenCode(TokenCode.CLASS, TokenCode.IDENTIFIER);
            matchTokenCode(TokenCode.IDENTIFIER, TokenCode.LBRACE);
            matchTokenCode(TokenCode.LBRACE, NonTerminals.VARIABLE_DECLARATIONS);
            variable_declarations();
            method_declarations();
            matchTokenCode(TokenCode.RBRACE);
        } else {
            noMatch();
        }
        m_CallStack.pop();
    }


    private void variable_declarations() {
        m_CallStack.push(NonTerminals.VARIABLE_DECLARATIONS);
        if (checkTokenCode(TokenCode.INT) || checkTokenCode(TokenCode.REAL) ) {
            type();
            variable_list();
            matchTokenCode(TokenCode.SEMICOLON, NonTerminals.VARIABLE_DECLARATIONS);
            variable_declarations();
        }//ef tad vantar int, fer hann beint i statement ss 'i stadinn fyrir int i; er i;
        m_CallStack.pop();
    }
    private void type() {
        m_CallStack.push(NonTerminals.TYPE);
        if (checkTokenCode(TokenCode.INT)) {
            matchTokenCode(TokenCode.INT);
        } else if (checkTokenCode(TokenCode.REAL)) {
            matchTokenCode(TokenCode.REAL);
        } else {
            noMatch();
        }
        m_CallStack.pop();
    }
    private void variable_list() {
        m_CallStack.push(NonTerminals.VARIABLE_LIST);
        if (checkTokenCode(TokenCode.IDENTIFIER)) {
            variable();
            variable_list_2();
        } else {
            noMatch();
        }
        m_CallStack.pop();

    }
    private void variable_list_2() {
        m_CallStack.push(NonTerminals.VARIABLE_LIST_2);
        if (checkTokenCode(TokenCode.COMMA)) {
            matchTokenCode(TokenCode.COMMA, NonTerminals.VARIABLE);
            variable();
            variable_list_2();
        }
        m_CallStack.pop();

    }
    private void variable() {
        m_CallStack.push(NonTerminals.VARIABLE);
        if (checkTokenCode(TokenCode.IDENTIFIER)) {
            matchTokenCode(TokenCode.IDENTIFIER, NonTerminals.VARIABLE_2);
            variable_2();
        } else {
            noMatch();
        }
        m_CallStack.pop();
    }
    private void variable_2() {
        m_CallStack.push(NonTerminals.VARIABLE_2);
        if (checkTokenCode(TokenCode.LBRACKET)) {
            matchTokenCode(TokenCode.LBRACKET, TokenCode.NUMBER);
            matchTokenCode(TokenCode.NUMBER, TokenCode.RBRACKET);
            matchTokenCode(TokenCode.RBRACKET);
        }
        m_CallStack.pop();
    }
    private void method_declarations() {
        m_CallStack.push(NonTerminals.METHOD_DECLARATIONS);
        if (checkTokenCode(TokenCode.STATIC)) {
            method_declaration();
            more_method_declarations();
        } else {
            noMatch();
        }
        m_CallStack.pop();
    }

    private void more_method_declarations() {
        m_CallStack.push(NonTerminals.MORE_METHOD_DECLARATIONS);
        if (checkTokenCode(TokenCode.STATIC)) {
            method_declaration();
            more_method_declarations();
        }
        m_CallStack.pop();

    }
    private void method_declaration() {
        m_CallStack.push(NonTerminals.METHOD_DECLARATION);
        if (checkTokenCode(TokenCode.STATIC)) {
            matchTokenCode(TokenCode.STATIC, NonTerminals.METHOD_RETURN_TYPE);
            method_return_type();
            matchTokenCode(TokenCode.IDENTIFIER, TokenCode.LPAREN);
            matchTokenCode(TokenCode.LPAREN, NonTerminals.PARAMETERS);
            parameters();
            matchTokenCode(TokenCode.RPAREN, TokenCode.LBRACE);
            matchTokenCode(TokenCode.LBRACE, NonTerminals.VARIABLE_DECLARATIONS);
            variable_declarations();
            statement_list();
            matchTokenCode(TokenCode.RBRACE);
        } else {
            noMatch();
        }
        m_CallStack.pop();
    }
    private void method_return_type () {
        m_CallStack.push(NonTerminals.METHOD_RETURN_TYPE);
        if (checkTokenCode(TokenCode.INT) || checkTokenCode(TokenCode.REAL)) {
            type();
        } else if (checkTokenCode(TokenCode.VOID)){
            matchTokenCode(TokenCode.VOID);
        } else {
            noMatch();
        }
        m_CallStack.pop();
    }

    private void parameters() {
        m_CallStack.push(NonTerminals.PARAMETERS);
        if (checkTokenCode(TokenCode.INT) || (checkTokenCode(TokenCode.REAL))) {
            parameter_list();
        }
        m_CallStack.pop();
    }

    private void parameter_list() {
        m_CallStack.push(NonTerminals.PARAMETER_LIST);
        if (checkTokenCode(TokenCode.INT) || checkTokenCode(TokenCode.REAL)) {
            type();
            matchTokenCode(TokenCode.IDENTIFIER, NonTerminals.PARAMETER_LIST_2);
            parameter_list_2();
        } else {
            noMatch();
        }
        m_CallStack.pop();
    }

    private void parameter_list_2() {
        m_CallStack.push(NonTerminals.PARAMETER_LIST_2);
        if (checkTokenCode(TokenCode.COMMA)) {
            matchTokenCode(TokenCode.COMMA, NonTerminals.TYPE);
            type();
            matchTokenCode(TokenCode.IDENTIFIER, NonTerminals.PARAMETER_LIST_2);
            parameter_list_2();
        }
        m_CallStack.pop();
    }
    private void statement_list() {
        m_CallStack.push(NonTerminals.STATEMENT_LIST);
       // ε, id, if, for, return, break, continue, {
        if (checkTokenCode(TokenCode.IDENTIFIER) || checkTokenCode(TokenCode.IF) || checkTokenCode(TokenCode.FOR)
         || checkTokenCode(TokenCode.RETURN) || checkTokenCode(TokenCode.BREAK) || checkTokenCode(TokenCode.CONTINUE) || checkTokenCode(TokenCode.LBRACE)) {
//vantar int , sleppir rest af forritinu
            statement();
            statement_list();
        }
        m_CallStack.pop();
    }
    private void statement() {
        m_CallStack.push(NonTerminals.STATEMENT);
        if (checkTokenCode(TokenCode.IDENTIFIER)) {
            matchTokenCode(TokenCode.IDENTIFIER, NonTerminals.SUPER_AWESOME);
            super_awesome();

        } else if (checkTokenCode(TokenCode.IF)) {
            matchTokenCode(TokenCode.IF);
            matchTokenCode(TokenCode.LPAREN, NonTerminals.EXPRESSION);
            expression();
            matchTokenCode(TokenCode.RPAREN, NonTerminals.STATEMENT_BLOCK);
            statement_block();
            optional_else();

        } else if (checkTokenCode(TokenCode.FOR)) {
            matchTokenCode(TokenCode.FOR);
            matchTokenCode(TokenCode.LPAREN, NonTerminals.VARIABLE_LOC_ORIGINAL);
            variable_loc_orginal();
            matchTokenCode(TokenCode.ASSIGNOP, NonTerminals.EXPRESSION);
            expression();
            matchTokenCode(TokenCode.SEMICOLON, NonTerminals.EXPRESSION);
            expression();
            matchTokenCode(TokenCode.SEMICOLON, NonTerminals.INCR_DECR_VAR_ORIGINAL);
            incr_decr_var_orginal();
            matchTokenCode(TokenCode.RPAREN, NonTerminals.STATEMENT_BLOCK);
            statement_block();

        }  else if (checkTokenCode(TokenCode.RETURN)) {
            matchTokenCode(TokenCode.RETURN, NonTerminals.OPTIONAL_EXPRESSION);
            optional_expression();
            matchTokenCode(TokenCode.SEMICOLON);

        }  else if (checkTokenCode(TokenCode.BREAK)) {
            matchTokenCode(TokenCode.BREAK, TokenCode.SEMICOLON);
            matchTokenCode(TokenCode.SEMICOLON);

        }  else if (checkTokenCode(TokenCode.CONTINUE)) {
            matchTokenCode(TokenCode.CONTINUE, TokenCode.SEMICOLON);
            matchTokenCode(TokenCode.SEMICOLON);

        } else if (checkTokenCode(TokenCode.LBRACE)) {
            statement_block();
        } else {
            noMatch();
        }
        m_CallStack.pop();
    }

    private void super_awesome() {
        m_CallStack.push(NonTerminals.SUPER_AWESOME);
        if (checkTokenCode(TokenCode.LPAREN)) {
            matchTokenCode(TokenCode.LPAREN, NonTerminals.EXPRESSION_LIST);
            expression_list();
            matchTokenCode(TokenCode.RPAREN, TokenCode.SEMICOLON);
            matchTokenCode(TokenCode.SEMICOLON);
        } else if (checkTokenCode(TokenCode.ASSIGNOP) || checkTokenCode(TokenCode.LBRACKET) || checkTokenCode(TokenCode.INCDECOP)) {
            variable_loc();
            super_awesome_3();
        } else if (checkTokenCode(TokenCode.ADDOP) || checkTokenCode(TokenCode.SEMICOLON)) {
            noMatch();
        }
        m_CallStack.pop();

    }

    private void super_awesome_2() {
        m_CallStack.push(NonTerminals.SUPER_AWESOME_2);
        if (checkTokenCode(TokenCode.LPAREN)) {
            matchTokenCode(TokenCode.LPAREN, NonTerminals.EXPRESSION_LIST);
            expression_list();
            matchTokenCode(TokenCode.RPAREN);
        } else if (checkTokenCode(TokenCode.LBRACKET)) {
            variable_loc();
        }
        m_CallStack.pop();
    }

    private void super_awesome_3(){
        m_CallStack.push(NonTerminals.SUPER_AWESOME_3);
        if(checkTokenCode(TokenCode.ASSIGNOP)){
            matchTokenCode(TokenCode.ASSIGNOP, NonTerminals.EXPRESSION);
            expression();
            matchTokenCode(TokenCode.SEMICOLON);

        } else if(checkTokenCode(TokenCode.INCDECOP)){
            matchTokenCode(TokenCode.INCDECOP, TokenCode.SEMICOLON);
            matchTokenCode(TokenCode.SEMICOLON);
        } else {
            noMatch();
        }
        m_CallStack.pop();
    }
    private void optional_expression () {
        m_CallStack.push(NonTerminals.OPTIONAL_EXPRESSION);
        if (checkTokenCode(TokenCode.IDENTIFIER) || checkTokenCode(TokenCode.NUMBER) ||
                checkTokenCode(TokenCode.LPAREN) || checkTokenCode(TokenCode.NOT) ||
                checkTokenCode(TokenCode.ADDOP) ) {
            expression();
        }
        m_CallStack.pop();
    }
    private void statement_block() {
        m_CallStack.push(NonTerminals.STATEMENT_BLOCK);
        if (checkTokenCode(TokenCode.LBRACE)) {
            matchTokenCode(TokenCode.LBRACE, NonTerminals.STATEMENT_LIST);
            statement_list();
            matchTokenCode(TokenCode.RBRACE);
        } else {
            noMatch();
        }
        m_CallStack.pop();
    }

    private void incr_decr_var_orginal() {
        m_CallStack.push(NonTerminals.VARIABLE_LOC_ORIGINAL);
        if (checkTokenCode(TokenCode.IDENTIFIER)) {
            variable_loc_orginal();
            matchTokenCode(TokenCode.INCDECOP);
        } else {
            noMatch();
        }
        m_CallStack.pop();
    }
    private void optional_else() {
        m_CallStack.push(NonTerminals.OPTIONAL_ELSE);
        if (checkTokenCode(TokenCode.ELSE)) {
            matchTokenCode(TokenCode.ELSE, NonTerminals.STATEMENT_BLOCK);
            statement_block();
        }
        m_CallStack.pop();
    }
    private void expression_list() {
        m_CallStack.push(NonTerminals.EXPRESSION_LIST);
        if (checkTokenCode(TokenCode.IDENTIFIER) || checkTokenCode(TokenCode.NUMBER) ||
                checkTokenCode(TokenCode.LPAREN) || checkTokenCode(TokenCode.NOT) ||
                checkTokenCode(TokenCode.ADDOP) ) {
            expression();
            more_expressions();
        }
        m_CallStack.pop();
    }
    private void more_expressions() {
        m_CallStack.push(NonTerminals.MORE_EXPRESSIONS);
        if (checkTokenCode(TokenCode.COMMA)) {
            matchTokenCode(TokenCode.COMMA, NonTerminals.EXPRESSION);
            expression();
            more_expressions();
        }
        m_CallStack.pop();
    }
    private void expression() {
        m_CallStack.push(NonTerminals.EXPRESSION);
        if (checkTokenCode(TokenCode.IDENTIFIER) || checkTokenCode(TokenCode.NUMBER) ||
                checkTokenCode(TokenCode.LPAREN) || checkTokenCode(TokenCode.NOT) ||
                checkTokenCode(TokenCode.ADDOP) ) {
            simple_expression();
            expression_2();
        } else {
            noMatch();
        }
        m_CallStack.pop();
    }
    private void expression_2 () {
        m_CallStack.push(NonTerminals.EXPRESSION_2);
        if (checkTokenCode(TokenCode.RELOP)) {
            matchTokenCode(TokenCode.RELOP, NonTerminals.SIMPLE_EXPRESSION);
            simple_expression();
        }
        m_CallStack.pop();
    }
    private void simple_expression () {
        m_CallStack.push(NonTerminals.SIMPLE_EXPRESSION);
        if (checkTokenCode(TokenCode.ADDOP)) {
            sign();
            term();
            simple_expression_2();
        } else if (checkTokenCode(TokenCode.IDENTIFIER) || checkTokenCode(TokenCode.NUMBER) ||
                checkTokenCode(TokenCode.LPAREN) || checkTokenCode(TokenCode.NOT)) {
            term();
            simple_expression_2();
        } else {
            noMatch();
        }
        m_CallStack.pop();
    }
    private void simple_expression_2 () {
        m_CallStack.push(NonTerminals.SIMPLE_EXPRESSION_2);
        if (checkTokenCode(TokenCode.ADDOP)) {
            matchTokenCode(TokenCode.ADDOP, NonTerminals.TERM);
            term();
            simple_expression_2();
        }
        m_CallStack.pop();
    }
    private void term() {
        m_CallStack.push(NonTerminals.TERM);
        if (checkTokenCode(TokenCode.IDENTIFIER) || checkTokenCode(TokenCode.NUMBER) ||
                checkTokenCode(TokenCode.LPAREN) || checkTokenCode(TokenCode.NOT)) {
            factor();
            term_2();
        } else {
            noMatch();
        }
        m_CallStack.pop();
    }
    private void term_2() {
        m_CallStack.push(NonTerminals.TERM_2);
        if (checkTokenCode(TokenCode.MULOP)) {
            matchTokenCode(TokenCode.MULOP, NonTerminals.FACTOR);
            factor();
            term_2();
        }
        m_CallStack.pop();
    }
    private void factor() {
        m_CallStack.push(NonTerminals.FACTOR);
        if (checkTokenCode(TokenCode.NUMBER)) {
            matchTokenCode(TokenCode.NUMBER);
        } else if (checkTokenCode(TokenCode.IDENTIFIER)) {
            matchTokenCode(TokenCode.IDENTIFIER, NonTerminals.SUPER_AWESOME_2);
            super_awesome_2();
        } else if (checkTokenCode(TokenCode.NOT)) {
            matchTokenCode(TokenCode.NOT, NonTerminals.FACTOR);
            factor();
        } else if (checkTokenCode(TokenCode.LPAREN)) {
            matchTokenCode(TokenCode.LPAREN, NonTerminals.EXPRESSION);
            expression();
            matchTokenCode(TokenCode.RPAREN);
        } else {
            noMatch();
        }
        m_CallStack.pop();
    }
    private void variable_loc() {
        m_CallStack.push(NonTerminals.VARIABLE_LOC);
        if (checkTokenCode(TokenCode.LBRACKET)) {
            matchTokenCode(TokenCode.LBRACKET, NonTerminals.EXPRESSION);
            expression();
            matchTokenCode(TokenCode.RBRACKET);
        }
        m_CallStack.pop();
    }
    private void variable_loc_orginal() {
      m_CallStack.push(NonTerminals.VARIABLE_LOC_ORIGINAL);
      if (checkTokenCode(TokenCode.IDENTIFIER)) {
          matchTokenCode(TokenCode.IDENTIFIER, NonTerminals.VARIABLE_LOC_ORIGINAL_2);
          variable_loc_orginal_2();
      } else {
          noMatch();
      }
      m_CallStack.pop();
    }
    private void variable_loc_orginal_2() {
        m_CallStack.push(NonTerminals.VARIABLE_LOC_ORIGINAL_2);
        if (checkTokenCode(TokenCode.LBRACKET)) {
            matchTokenCode(TokenCode.LBRACKET, NonTerminals.EXPRESSION);
            expression();
            matchTokenCode(TokenCode.RBRACKET);
        }
        m_CallStack.pop();
    }

    private void sign() {
        m_CallStack.push(NonTerminals.SIGN);
        if (checkTokenCode(TokenCode.ADDOP)) {
            matchTokenCode(TokenCode.ADDOP);
        } else {
            noMatch();
        }
        m_CallStack.pop();
    }

    private void noMatch() {

        addError(TokenCode.NOMATCH, lookahead.getTokenCode(),  lookahead.getM_line(), lookahead.getM_column());

        m_synchronizedSet.addAll(m_follow.getFOLLOW(m_CallStack.peek()));
        m_synchronizedSet.addAll(m_first.getFIRST(m_CallStack.peek()));
        while(true) {
            for (TokenCode tokenCode : m_synchronizedSet) {
                if (lookahead.getTokenCode().equals(tokenCode)) {
                    m_synchronizedSet.clear();
                    return;
                }
            }
            lookahead = m_Token_Stack.poll();
        }
    }

    public void error(TokenCode t) {

        addError(t, lookahead.getTokenCode(), lookahead.getM_line(), lookahead.getM_column());

        m_synchronizedSet.addAll(m_follow.getFOLLOW(m_CallStack.peek()));
        while(true) {
            for (TokenCode tokenCode : m_synchronizedSet) {
                if (lookahead.getTokenCode().equals(tokenCode)) {
                    m_synchronizedSet.clear();
                    return;
                }
            }
            lookahead = m_Token_Stack.poll();
        }
    }

    public void matchTokenCode(TokenCode t) {
        if (t.equals(lookahead.getTokenCode())) {
            lookahead = m_Token_Stack.poll();
        } else {
            error(t);
        }
    }

    private void matchTokenCode(TokenCode t, TokenCode extra) {
        if (t.equals(lookahead.getTokenCode())) {
            lookahead = m_Token_Stack.poll();
        } else {
            m_synchronizedSet.add(extra);
            error(t);
        }
    }

    private void matchTokenCode(TokenCode t, NonTerminals nonTerminal) {
        if (t.equals(lookahead.getTokenCode())) {
            lookahead = m_Token_Stack.poll();
        } else {
            m_synchronizedSet.addAll(m_first.getFIRST(nonTerminal));
            error(t);
        }
    }

    public Boolean checkTokenCode(TokenCode t) {
        if (t.equals(lookahead.getTokenCode())) {
            lastCheck = t;
            return true;
        } else {
        //   System.out.println("PARSING ERROR IN CHECKTOKENCODE  " + t.toString());
            return false;
        }
    }

    private void addError(TokenCode exp, TokenCode got, int line, int column){
        ErrorHandler temp = new ErrorHandler(exp,got,line,column);
        numErrors++;
        m_errors.add(temp);
    }

    public List<ErrorHandler> getErrorList(){
        return m_errors;
    }
}
